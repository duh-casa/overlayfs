#!/bin/bash
# put this anywhere, for example in /root
# This script synchronises your runtime overlay with your rofs, allowing you to remove the overlay without loosing changes
mount -o remount,rw /rofs
rsync -axAX --info=progress2 --delete / /rofs