#!/bin/bash
wget https://gitlab.com/duh-casa/overlayfs/raw/master/etc_initramfs-tools_hooks_overlay?inline=false -O /mnt/boot/etc/initramfs-tools/hooks/overlay
wget https://gitlab.com/duh-casa/overlayfs/raw/master/etc_initramfs-tools_scripts_init-bottom_overlay?inline=false -O /mnt/boot/etc/initramfs-tools/scripts/init-bottom/overlay
chmod a+x /mnt/boot/etc/initramfs-tools/hooks/overlay
chmod a+x /mnt/boot/etc/initramfs-tools/scripts/init-bottom/overlay