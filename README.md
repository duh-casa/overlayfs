OverlayFS for Progressive Linux Installer
=========================================

These are the overlayfs initramfs-tools scripts, for the [Progressive Linux Installer idea](https://gitlab.com/duh-casa/RV-disks/blob/master/doc/Progressive%20install%20CD.md).

They are based off of the [Gist snippet by mutability](https://gist.github.com/mutability/6cc944bde1cf4f61908e316befd42bc4), as well as [Casper sources](https://git.launchpad.net/ubuntu/+source/casper/tree/scripts/casper#n412), [a helpful StackExchange answer by kksturi](https://unix.stackexchange.com/a/445141/187949), official documentation and several other sources.

Implementation
--------------

The Progressive Linux Installer creates two ext4 partitions which are to be used in a overlayfs arrangement, allowing the lower filesystem to be mounted read-only for [zerofree](http://manpages.ubuntu.com/manpages/xenial/man8/zerofree.8.html), while the upper filesystem is later erased.

These are:
* Lower: `/dev/sda1`, e2labelled "boot"
* Upper: `/dev/sda2`, e2labelled "install"

Required directories:
* "boot" needs: `/rofs` and `/cow`
* "install" needs: `/rw` and `/work`

The scripts rely on the existence of these labels and directories, so you must prepare them beforehand.

After that, simply place the overlay files into the correct directories in `/etc/initramfs-tools/` and run `sudo update-initramfs -u`.

If you are updating a system that is already using this overlay, do not forget to synchronise your rofs before rebooting.

Removal
-------

To remove the overlay, consider running the supplied [synchronisation script](synchronise.sh), to merge your changes back to your rofs.

Afterwards, simply remove the two files you added to `/etc/initramfs-tools` and run `sudo update-initramfs -u` again.
